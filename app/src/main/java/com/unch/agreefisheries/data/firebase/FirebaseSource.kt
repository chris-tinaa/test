package com.unch.agreefisheries.data.firebase

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.unch.agreefisheries.ui.auth.VerificationActivity
import com.unch.agreefisheries.ui.home.HomeActivity
import com.unch.agreefisheries.util.startVerificationActivity
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.SchedulerSupport.IO
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.util.concurrent.TimeUnit
import kotlin.math.log

class FirebaseSource {
    private val firebaseAuth: FirebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }

    fun loginRegisterPhoneNumber(phoneNumber: String, context: Activity) =
        Completable.create { emitter ->

            if (phoneNumber[0].equals('0')) {
                phoneNumber.removeRange(0, 1)
            } else if (phoneNumber.substring(0, 3).equals("+62")) {
                phoneNumber.removeRange(0, 3)
            } else if (phoneNumber.substring(0, 2).equals("62")) {
                phoneNumber.removeRange(0, 2)
            }

            val callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

                override fun onVerificationCompleted(credential: PhoneAuthCredential) {

                    //Log.d(TAG, "onVerificationCompleted:$credential")
                    if (!credential.smsCode.isNullOrEmpty()) {
                        Log.e("onVerificationCompleted", credential.smsCode!!)
                    }
                }

                override fun onVerificationFailed(e: FirebaseException) {
                    if (e is FirebaseAuthInvalidCredentialsException) {
                        Log.e("invalidCredential", e.toString());
                    } else if (e is FirebaseTooManyRequestsException) {
                        Log.e("out of quota", e.toString());
                    }
                }

                override fun onCodeAutoRetrievalTimeOut(p0: String) {
                    super.onCodeAutoRetrievalTimeOut(p0)
                    Log.e("", p0)
                }

                override fun onCodeSent(
                    verificationId: String,
                    token: PhoneAuthProvider.ForceResendingToken
                ) {
                    // The SMS verification code has been sent to the provided phone number, we
                    // now need to ask the user to enter the code and then construct a credential
                    // by combining the code with a verification ID.
                    //Log.d(TAG, "onCodeSent:$verificationId")

                    // Save verification ID and resending token so we can use them later

                    if (!emitter.isDisposed) {
                        Log.e("", "onCodeSent:" + verificationId);
                        var verifId = verificationId
//                        var mResendToken = token
//                        var intent = Intent(this, VerificationActivity::class.java)
//                        intent.putExtra("verifId", verifId)
//                        startActivity(intent)
//                        finish()
//
                        context.startVerificationActivity(
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK,
                            "verifId",
                            verifId)

                        emitter.onComplete()
                    }
                }
            }


            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+62" + phoneNumber,
                10,
                TimeUnit.SECONDS,
                context,
                callbacks
            )
        }

    fun loginEmail(email: String, password: String) = Completable.create { emitter ->
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
            if (!emitter.isDisposed) {
                if (it.isSuccessful()) {
                    emitter.onComplete()
                } else {
                    emitter.onError(it.exception!!)
                }
            }
        }
    }

    fun sendVerificationEmail() = Completable.create { emitter ->
        var user = currentUser()

        Log.i("sendEmailVerif", "STARTING")

        if (user != null) {
            user.sendEmailVerification().addOnCompleteListener {
                if (!emitter.isDisposed) {
                    if (it.isSuccessful) {
                        logout()
                        emitter.onComplete()
                        Log.i("sendEmailVerif", "SUCCESS")
                    } else {
                        emitter.onError(it.exception!!)
                        Log.i("sendEmailVerif", "FAIL")
                        //restart this activity
//                        overridePendingTransition(0, 0);
//                        finish();
//                        overridePendingTransition(0, 0);
//                        startActivity(getIntent());
                    }
                }
            }
        }

    }


    fun registerEmail(email: String, password: String) = Completable.create { emitter ->

        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener {
            if (!emitter.isDisposed) {
                if (it.isSuccessful()) {
                    emitter.onComplete()
                } else {
                    Log.w("RegisFail", "createUserWithEmail:failure", it.getException())
                    emitter.onError(it.exception!!)
                }
            }
        }
    }


    fun logout() = firebaseAuth.signOut()

    fun currentUser() = firebaseAuth.currentUser
}