package com.unch.agreefisheries.data.repositories

import android.app.Activity
import com.google.firebase.auth.FirebaseAuth
import com.unch.agreefisheries.data.firebase.FirebaseSource

class UserRepository(
    private val firebase: FirebaseSource
) {
    fun loginRegisterPhoneNumber(phoneNumber: String, context: Activity) = firebase.loginRegisterPhoneNumber(phoneNumber, context)

    fun loginEmail(email: String, password: String) = firebase.loginEmail(email, password)

    fun sendVerificationEmail() = firebase.sendVerificationEmail()

    fun registerEmail(email: String, password: String) = firebase.registerEmail(email, password)

    fun currentUser() = firebase.currentUser()
}