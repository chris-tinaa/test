package com.unch.agreefisheries.ui.auth

interface AuthListener {
    fun onStarted()
    fun onSuccess()
    fun onFailure(message: String)
    fun loginFinish()
    fun registerFinish()
    fun sendToast(message: String)
}