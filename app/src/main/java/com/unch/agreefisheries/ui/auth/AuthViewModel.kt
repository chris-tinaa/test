package com.unch.agreefisheries.ui.auth

import android.app.Activity
import android.app.Application
import android.view.View
import androidx.lifecycle.ViewModel
import com.unch.agreefisheries.data.repositories.UserRepository
import com.unch.agreefisheries.util.isEmailValid
import com.unch.agreefisheries.util.isNameValid
import com.unch.agreefisheries.util.isPasswordValid
import com.unch.agreefisheries.util.isPhoneNumber
import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.Schedulers.io
import kotlinx.android.synthetic.main.activity_login.*

class AuthViewModel(
    private val repository: UserRepository
) : ViewModel() {

    var nohp_email: String? = null
    var sandi: String? = null
    var profesi: String? = null
    var rb_nelayan: Boolean = false
    var rb_pembudidaya: Boolean = false
    var nama: String? = null
    var konfirmasi_sandi: String? = null
    var cb_syaratketentuan: Boolean = false
    var authListener: AuthListener? = null

    private val disposables = CompositeDisposable()

    val user by lazy {
        repository.currentUser()
    }

    fun login(view: View) {

        var loginFunc: Completable

        if (nohp_email.isNullOrEmpty() || sandi.isNullOrEmpty()) {
            authListener?.onFailure("Isi data dengan lengkap.")
            return
        }

        if (nohp_email!!.isEmailValid()) {
            loginFunc = repository.loginEmail(nohp_email!!, sandi!!)
        } else if (nohp_email!!.isPhoneNumber()) {
            loginFunc = repository.loginRegisterPhoneNumber(nohp_email!!, view.context as Activity)
        } else {
            authListener?.onFailure("Masukkan email/no. handphone yang valid.")
            return
        }

        authListener?.onStarted()

        val disposable = loginFunc
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (!nohp_email!!.isPhoneNumber()) {
                    if (user != null) {
                        if (user!!.isEmailVerified) {
                            authListener?.onSuccess()
                            authListener?.loginFinish()
                        } else {
                            sendVerificationEmail()
                            authListener?.sendToast("Verifikasi email terlebih dahulu!")
                        }
                    }
                } else {
                    authListener?.sendToast("Kode OTP segera dikirim lewat SMS")
                }
            }, {
                authListener?.onFailure(it.message!!)
            })


        disposables.add(disposable)

    }

    fun register(view: View) {

        val regisFunc : Completable

        if ((!rb_nelayan && !rb_pembudidaya) ||
            nama.isNullOrEmpty() ||
            nohp_email.isNullOrEmpty() ||
            sandi.isNullOrEmpty() ||
            konfirmasi_sandi.isNullOrEmpty() ||
            !cb_syaratketentuan
        ) {
            authListener?.onFailure("Isi data dengan lengkap.")
            return
        }

        if (!nama!!.isNameValid()){
            authListener?.onFailure("Masukkan nama yang valid")
            return
        }

        if (!sandi!!.isPasswordValid()){
            authListener?.onFailure("Minimal 8 karakter, terdiri dari kombinasi huruf kecil, huruf kapital, dan angka")
            return
        }

        if (!sandi!!.equals(konfirmasi_sandi!!)){
            authListener?.onFailure("Kata sandi tidak sesuai.")
            return
        }

        if (nohp_email!!.isEmailValid()) {
            regisFunc = repository.registerEmail(nohp_email!!, sandi!!)
        } else if (nohp_email!!.isPhoneNumber()) {
            regisFunc = repository.loginRegisterPhoneNumber(nohp_email!!, view.context as Activity)
        } else {
            authListener?.onFailure("Masukkan email/no. handphone yang valid.")
            return
        }


        val disposable = regisFunc
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (!nohp_email!!.isPhoneNumber()) {
                    sendVerificationEmail()
                    authListener?.sendToast("Email verifikasi segera dikirim. Cek email untuk verifikasi akun.")
                    authListener?.registerFinish()
                } else {
                    authListener?.sendToast("Kode OTP segera dikirim lewat SMS")
                }
            }, {
                authListener?.onFailure(it.message!!)
            })
        disposables.add(disposable)
    }

    fun sendVerificationEmail(){

        val disposable = repository.sendVerificationEmail()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({

            }, {
                authListener?.onFailure(it.message!!)
            })

        disposables.add(disposable)

    }
}