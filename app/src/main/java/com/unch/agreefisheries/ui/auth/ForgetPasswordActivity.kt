package com.unch.agreefisheries.ui.auth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.unch.agreefisheries.R
import kotlinx.android.synthetic.main.activity_forget_password.*

class ForgetPasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_password)

        btn_send.setOnClickListener {
            FirebaseAuth.getInstance().sendPasswordResetEmail(et_email.text.toString())
                .addOnCompleteListener(OnCompleteListener<Void> {
                    if (it.isSuccessful){
                        Toast.makeText(this, "Silakan cek email anda.", Toast.LENGTH_LONG).show()
                        finish()
                    }
                })
        }
    }
}
