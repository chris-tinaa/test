package com.unch.agreefisheries.ui.auth


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.unch.agreefisheries.R
import com.unch.agreefisheries.databinding.ActivityLoginBinding
import com.unch.agreefisheries.util.startHomeActivity
import com.unch.agreefisheries.util.startLoginActivity
import com.unch.agreefisheries.util.startRegisterActivity
import com.unch.agreefisheries.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class LoginActivity : AppCompatActivity(), AuthListener, KodeinAware {

    override val kodein by kodein()
    private val factory: AuthViewModelFactory by instance<AuthViewModelFactory>()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)

        val binding: ActivityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        val viewModel = ViewModelProviders.of(this, factory).get(AuthViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel.authListener = this

        binding.tvToRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }

        binding.tvForgetPassword.setOnClickListener {
            startActivity(Intent(this, ForgetPasswordActivity::class.java))
        }

    }


    override fun onStarted() {
        toast("Sedang memproses")
    }

    override fun onSuccess() {
        toast("Berhasil")
    }

    override fun onFailure(message: String) {
        toast(message)
    }

    override fun loginFinish() {
        startHomeActivity(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
    }

    override fun registerFinish() {
        startLoginActivity(0)
        finish()
    }

    override fun sendToast(message: String){
        toast(message)
    }
}
