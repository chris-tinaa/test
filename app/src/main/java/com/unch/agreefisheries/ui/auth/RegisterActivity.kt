package com.unch.agreefisheries.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.unch.agreefisheries.R
import com.unch.agreefisheries.databinding.ActivityRegisterBinding
import com.unch.agreefisheries.ui.auth.VerificationActivity
import com.unch.agreefisheries.util.startHomeActivity
import com.unch.agreefisheries.util.startLoginActivity
import com.unch.agreefisheries.util.toast
import io.reactivex.Completable
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.et_nohp_email
import kotlinx.android.synthetic.main.activity_register.et_sandi
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern
import org.kodein.di.android.x.kodein

class RegisterActivity : AppCompatActivity(), AuthListener, KodeinAware {

    override val kodein by kodein()
    private val factory: AuthViewModelFactory by instance<AuthViewModelFactory>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val binding: ActivityRegisterBinding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        val viewModel = ViewModelProviders.of(this, factory).get(AuthViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel.authListener = this

        binding.tvToLogin.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    override fun onStarted() {
        toast("Sedang memproses")
    }

    override fun onSuccess() {
        toast("Berhasil")
    }

    override fun onFailure(message: String) {
        toast(message)
    }

    override fun loginFinish() {
        startHomeActivity(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        toast("Berhasil masuk")
    }

    override fun registerFinish() {
        startLoginActivity(0)
        finish()
    }

    override fun sendToast(message: String){
        toast(message)
    }
}
