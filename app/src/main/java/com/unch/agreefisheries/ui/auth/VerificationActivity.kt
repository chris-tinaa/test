package com.unch.agreefisheries.ui.auth

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.*
import com.unch.agreefisheries.ui.home.HomeActivity
import com.unch.agreefisheries.R
import kotlinx.android.synthetic.main.activity_verification.*

class VerificationActivity : AppCompatActivity() {

    lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)

        mAuth = FirebaseAuth.getInstance()
        var verifId = intent.getStringExtra("verifId")

        btn_verify.setOnClickListener {
            var otp = et_otp.text.toString().trim()
            if(!otp.isEmpty()){
                verifyOtp(verifId , otp);
            }else {
                et_otp.setError("Invalid otp");
            }

        }
    }

    fun verifyOtp(verifId: String, otp: String){
        var credential = PhoneAuthProvider.getCredential(verifId, otp)
        loginWithPhoneAuthCredential(credential);
    }

    fun loginWithPhoneAuthCredential(credential: PhoneAuthCredential){
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(OnCompleteListener<AuthResult> {
                if (it.isSuccessful){
                    var intent = Intent(this, HomeActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                } else {
                    Log.w("status", "signInWithCredential:failure", it.getException());
                    if (it.getException() is FirebaseAuthInvalidCredentialsException) {
                        Log.e("err", it.getException().toString());
                        Toast.makeText(this@VerificationActivity,"Kode Verifikasi Salah", Toast.LENGTH_SHORT).show();
                    }
                }
            })
    }
}
