package com.unch.agreefisheries.ui.onboarding

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.unch.agreefisheries.R
import com.unch.agreefisheries.ui.auth.LoginActivity
import com.unch.agreefisheries.ui.auth.RegisterActivity
import kotlinx.android.synthetic.main.activity_onboarding.*

class OnboardingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)

        onboardingSetup()

        btn_login.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }

        btn_register.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
    }

    private fun getDataList(): ArrayList<OnboardingModel>{
        val dataList = ArrayList<OnboardingModel>()
        val item1 = OnboardingModel(
            R.drawable.ic_onboarding_fishing,
            "Analisis & Monitoring",
            "Kelola seluruh aktivitas perikanan dengan mudah, kapanpun, dan dimanapun."
        )
        val item2 = OnboardingModel(
            R.drawable.ic_onboarding_checklist,
            "Produktifitas",
            "Kelola Hasil Produksi dan Distribusi Ikan dan Dapatkan Analisis Kelayakan Usaha"
        )
        val item3 = OnboardingModel(
            R.drawable.ic_onboarding_moneytree,
            "Permodalan",
            "Dapatkan akses bantuan dan pendanaan dengan berbagai pilihan."
        )

        dataList.add(item1)
        dataList.add(item2)
        dataList.add(item3)

        return dataList
    }

    private fun onboardingSetup(){
        val boardingItem =
            OnboardingAdapter(getDataList())

        onboardingViewPager.adapter = boardingItem
        indicator_onboarding.setViewPager(onboardingViewPager)

    }

}
