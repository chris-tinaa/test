package com.unch.agreefisheries.ui.onboarding

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.unch.agreefisheries.R
import kotlinx.android.synthetic.main.item_onboarding.view.*

class OnboardingAdapter(private val pagerList: List<OnboardingModel>): PagerAdapter(){
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return pagerList.size
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context).inflate(R.layout.item_onboarding, container, false)
        val onboarding_item = pagerList[position]
        view.apply {
            tv_title.text = onboarding_item.tv_title
            tv_desc.text = onboarding_item.tv_desc
            Glide.with(this)
                .load(onboarding_item.iv_picture)
                .into(iv_picture)
        }
        container.addView(view,0)
        return view
    }
}