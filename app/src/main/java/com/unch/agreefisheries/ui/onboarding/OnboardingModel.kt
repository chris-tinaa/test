package com.unch.agreefisheries.ui.onboarding

data class OnboardingModel(
    val iv_picture: Int,
    val tv_title: String,
    val tv_desc: String
)