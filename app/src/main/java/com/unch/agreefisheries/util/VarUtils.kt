package com.unch.agreefisheries.util

import android.text.TextUtils
import java.util.regex.Pattern


fun String.isPasswordValid(): Boolean {
    val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$"
    var pattern = Pattern.compile(PASSWORD_PATTERN)
    var matcher = pattern.matcher(this)

    return matcher.matches()

}

fun String.isEmailValid(): Boolean {
    return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this)
        .matches()
}

fun String.isPhoneNumber(): Boolean {
    return if (this.isNullOrEmpty()) false else this.all { Character.isDigit(it) }
}

fun String.isNameValid(): Boolean {
    val regex = "^[A-Za-z ]+$".toRegex()
    return regex.matches(this)
}
