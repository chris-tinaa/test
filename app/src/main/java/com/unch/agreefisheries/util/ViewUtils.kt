package com.unch.agreefisheries.util

import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.unch.agreefisheries.ui.auth.ForgetPasswordActivity
import com.unch.agreefisheries.ui.auth.LoginActivity
import com.unch.agreefisheries.ui.auth.RegisterActivity
import com.unch.agreefisheries.ui.auth.VerificationActivity
import com.unch.agreefisheries.ui.home.HomeActivity

fun Context.toast(message: String){
    Toast.makeText(this, message, Toast.LENGTH_LONG ).show()
}

fun ProgressBar.show(){
    visibility = View.VISIBLE
}

fun ProgressBar.hide(){
    visibility = View.GONE
}

fun View.snackbar(message: String){
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).also { snackbar ->
        snackbar.setAction("Ok") {
            snackbar.dismiss()
        }
    }.show()
}


fun Context.startHomeActivity(flags: Int) =
    Intent(this, HomeActivity::class.java).also {
        it.flags = flags
        startActivity(it)
    }

fun Context.startLoginActivity(flags: Int) =
    Intent(this, LoginActivity::class.java).also {
        it.flags = flags
        startActivity(it)
    }

fun Context.startRegisterActivity(flags: Int) =
    Intent(this, RegisterActivity::class.java).also {
        it.flags = flags
        startActivity(it)
    }

fun Context.startVerificationActivity(flags: Int, extraName: String, extraValue: String) =
    Intent(this, VerificationActivity::class.java).also {
        it.flags = flags
        it.putExtra(extraName,extraValue)
        startActivity(it)
    }

fun Context.startForgetPasswordActivity(flags: Int) =
    Intent(this, ForgetPasswordActivity::class.java).also {
        it.flags = flags
        startActivity(it)
    }

